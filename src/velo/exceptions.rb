# Copyright (c) 2012-2024, Chris Pressey, Cat's Eye Technologies.
# This file is distributed under a 2-clause BSD license.  See LICENSES/ dir.
# SPDX-License-Identifier: LicenseRef-BSD-2-Clause-X-Velo

require 'velo/debug'

class VeloSyntaxError < StandardError  
end

class VeloAttributeNotFound < StandardError
end

class VeloMethodNotImplemented < StandardError
end
